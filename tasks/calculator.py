def calculator(a, b, operator):
    # ==============
    # Your code here
    calculation = ()
    first_value = a
    second_value = b
    operation = operator
    if operation == "+":
        calculation = first_value + second_value
        return calculation
    if operation == "-":
        calculation = first_value - second_value
        return calculation
    if operation == "*":
        calculation = first_value * second_value
        return calculation
    if operation == "/":
        calculation = first_value // second_value
        return calculation







    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
